package loris.pack.managers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

public class FileManager 
{
	private static FileManager instance = null;
	
	private FileManager() {}
	
	public static FileManager GetInstance()
	{
		return instance == null ? new FileManager() : instance;
	}

	public FileConfiguration GetFileConfig()
	{
		File userdata = new File(Bukkit.getServer().getPluginManager().getPlugin("Pack").getDataFolder(), File.separator + "PlayerDatabase");
	    File f = new File(userdata, File.separator + "Inventories" + ".yml");
	    return YamlConfiguration.loadConfiguration(f);
	}
	
	public void SetFileConfig(String player, ItemStack[] items)
	{
		File userdata = new File(Bukkit.getServer().getPluginManager().getPlugin("Pack").getDataFolder(), File.separator + "PlayerDatabase");
	    File f = new File(userdata, File.separator + "Inventories" + ".yml");
	    FileConfiguration playerData = GetFileConfig();
	    
	    List<ItemStack> finalInv = new ArrayList<ItemStack>();
	    for (ItemStack item : items) 
	    {
			if(item != null)
			{
				finalInv.add(item);
			}
		}
	    
	    playerData.set("inventories." + player, finalInv.toArray());
	    
	    try
	    {
			playerData.save(f);
		} 
	    catch (java.io.IOException e) 
	    {
			e.printStackTrace();
		}
	}
	
}
