package loris.pack;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import loris.pack.GUI.PackGui;

public class Commands implements TabExecutor
{	
	private Plugin plugin;
	
	public Commands(Plugin plug) 
	{
		plugin = plug;
	}

	public List<String> onTabComplete(CommandSender arg0, Command arg1, String arg2, String[] arg3) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) 
	{
		// Only Players can do commands!
		if(!(sender instanceof Player)) return false;
		
		Player p = (Player) sender;
		
		String command = cmd.getName().toLowerCase();
		
		switch (command)
		{
			case "pack":
				if(args.length > 0)
				{
					if(p.hasPermission("pack.openOther"))
					{
						PackGui pg = PackGui.GetInstance(plugin);
						pg.OpenPackMenu(p, plugin, args[0]);
						break;
					}
					else
					{
						p.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command!");
						break;
					}
				}
				
				if(p.hasPermission("pack.open"))
				{
					PackGui pg = PackGui.GetInstance(plugin);
					pg.OpenPackMenu(p);
				}
				else
				{
					p.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command!");
				}
				break;
			case "packreload":
				if(p.hasPermission("pack.reload"))
				{
					p.sendMessage(ChatColor.YELLOW + "Reloading Plugin!");
					plugin.reloadConfig();
					plugin.onDisable();
					plugin.onEnable();
					p.sendMessage(ChatColor.GREEN + "Plugin Reloaded!");
				}
				else
				{
					p.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command!");
				}
				break;
		}
		
		return true;
	}
}