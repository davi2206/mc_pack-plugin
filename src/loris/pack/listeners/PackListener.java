package loris.pack.listeners;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import loris.pack.managers.FileManager;

public class PackListener implements Listener
{
	private static PackListener lobbyListener;
	private static HashMap<Player, String> targetPlayers;
	
	private Player player;
	
	private PackListener()
	{
		targetPlayers = new HashMap<Player, String>();
	}
	
	public void setTargetPlayer(Player player, String targetPlayerName)
	{
		targetPlayers.put(player, targetPlayerName);
	}
	
	public static PackListener getInstance()
	{
		return lobbyListener == null ? new PackListener() : lobbyListener;
	}
	
	@EventHandler
    public void onInventoryClick(InventoryClickEvent e) 
	{
		String invName = e.getView().getTitle().toString();
		
		String invTypeName = "";
		try
		{
			invTypeName = e.getClickedInventory().getType().name();
		}
		catch (Exception ex) 
		{
			return;
		}
		
		if(invName == "## Pack ##")
		{
			player = (Player) e.getWhoClicked();
						
			ItemStack clickedItem = e.getCurrentItem();
			ClickType ct = e.getClick();
			
			if(ct.name() == "DOUBLE_CLICK")
			{
				e.setCancelled(true);
				return;
			}
			
			// verify current item is not null
	        if (clickedItem == null) return;
			
	        if(!clickedItem.getType().equals(Material.AIR))
	        {
	        	String targetPlayer = targetPlayers.get(player);
	        	
	        	String permissionTake = (player.getName() == targetPlayer) ? "pack.take" : "pack.takeOther";
	        	String permissionPut = (player.getName() == targetPlayer) ? "pack.put" : "pack.putOther";
	        	
	        	if (invTypeName.equalsIgnoreCase("CHEST"))
				{
	        		if(!player.hasPermission(permissionTake))
	        	    {
			        	e.setCancelled(true);
			        	return;
			        }
				}
				else
				{
					if(!player.hasPermission(permissionPut))
			        {
			        	e.setCancelled(true);
			        	return;
			        }
				}
	        }
		}
	}
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e)
	{
		String invName = e.getView().getTitle().toString();
		
		if(invName == "## Pack ##")
		{
			player = (Player) e.getPlayer();
			String targetPlayer = targetPlayers.get(player);
			Inventory inv = e.getInventory();
			
			ItemStack[] is = inv.getContents();
			FileManager fm = FileManager.GetInstance();
			fm.SetFileConfig(targetPlayer, is);
		}
	}
}




