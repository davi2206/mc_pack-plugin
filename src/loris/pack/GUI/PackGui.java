package loris.pack.GUI;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import loris.pack.listeners.PackListener;
import loris.pack.managers.FileManager;

public class PackGui 
{
	private static PackGui instance;
	private static Plugin plugin;
	CreateGuiItems guiManager;
	FileManager fileMan;
	PackListener pl;
	
	private PackGui()
	{
		guiManager = CreateGuiItems.getInstance();
		fileMan = FileManager.GetInstance();
		pl = PackListener.getInstance();
	}
	
	public static PackGui GetInstance(Plugin plug)
	{
		plugin = plug;
		return instance == null ? new PackGui() : instance;
	}
	
	// Open own Pack
	public void OpenPackMenu(Player player)
	{
		try 
		{
			pl.setTargetPlayer(player, player.getName());
			
			int guiSize = plugin.getConfig().getInt("packrows");
			
			Inventory inv = guiManager.createInv((guiSize*9), "## Pack ##");

			FileConfiguration playerData = fileMan.GetFileConfig();
		    List<ItemStack> i = (List<ItemStack>)playerData.get("inventories."+player.getName());
		    
		    if(i != null)
		    {
		    	for (ItemStack itemStack : i) 
		    	{
					inv.addItem(itemStack);
				}
		    }
		    
		    player.openInventory(inv);
		}
		catch (Exception e) 
		{
			player.sendMessage(ChatColor.RED+"Something went wrong :( Ask an admin");
		}
	}
	
	// Open others Pack
	public void OpenPackMenu(Player player, Plugin plugin, String playerName)
	{
		try 
		{
			pl.setTargetPlayer(player, playerName);
			int guiSize = plugin.getConfig().getInt("packrows");
			
			Inventory inv = guiManager.createInv((guiSize*9), "## Pack ##");

			FileConfiguration playerData = fileMan.GetFileConfig();
		    List<ItemStack> i = (List<ItemStack>)playerData.get("inventories."+playerName);
		    
		    if(i != null)
		    {
		    	for (ItemStack itemStack : i) 
		    	{
					inv.addItem(itemStack);
				}
		    }
		    
		    player.openInventory(inv);
		}
		catch (Exception e) 
		{
			player.sendMessage(ChatColor.RED+"Something went wrong :( Ask an admin");
		}
	}
}
