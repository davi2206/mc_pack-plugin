package loris.pack.GUI;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CreateGuiItems 
{
	private static CreateGuiItems instance;
	
	private CreateGuiItems() {}
	
	public static CreateGuiItems getInstance()
	{
		return instance == null ? new CreateGuiItems() : instance;
	}
	
	/**
	 * Create a new Inventory instance
	 * @param size (Must be divisible with 9)
	 * @param name 
	 * @return
	 */
	public Inventory createInv(int size, String name)
	{
		return Bukkit.createInventory(null, size, name);
	}
	
	/**
	 * Create an item to put in the GUI
	 * @param name of the item
	 * @param desc of the item, multiline
	 * @param mat the material to use. (Material.[item])
	 * @param amount of the item
	 * @param id custom ID to be used in GUI menues
	 * @return the itemstack object
	 */
	public ItemStack createGuiItem(String name, ArrayList<String> desc, Material mat, int amount, int id)
	{
        ItemStack is = new ItemStack(mat, amount);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(name);
        im.setLore(desc);
        im.setCustomModelData(id);
        is.setItemMeta(im);
        return is;
    }
	
	/**
	 * Create an item to put in the GUI, with an enchantment
	 * @param name of the item
	 * @param desc of the item, multiline
	 * @param mat the material to use. (Material.[item])
	 * @param enc The enchantment to put on the item. (Enchantment.[name])
	 * @param enc_lvl The level of the enchantment
	 * @param amount of the item
	 * @param id custom ID to be used in GUI menues
	 * @return the itemstack object
	 */
	public ItemStack createGuiItemEnchant(String name, ArrayList<String> desc, Material mat, Enchantment enc, int enc_lvl, int amount, int id)
	{
        ItemStack is = new ItemStack(mat, amount);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(name);
        im.setLore(desc);
        im.setCustomModelData(id);
        is.setItemMeta(im);
        try
        {
        	is.addEnchantment(enc, enc_lvl);
        }
        catch (Exception e) {
			// TODO: handle exception
		}
        
        return is;
    }
}
