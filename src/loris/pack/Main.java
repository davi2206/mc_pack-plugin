package loris.pack;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.plugin.java.JavaPlugin;

import loris.pack.listeners.PackListener;

public class Main extends JavaPlugin
{
	private ConsoleCommandSender clog;
    
	@Override
	public void onEnable()
	{
		clog = this.getServer().getConsoleSender();
		        
        // Create or Load Config
		saveDefaultConfig();
        
        // Implement cmd's here
		Commands cmds = new Commands(this);
        getCommand("pack").setTabCompleter((TabCompleter) cmds);
        getCommand("pack").setExecutor((CommandExecutor) cmds);
        
        getCommand("packreload").setTabCompleter((TabCompleter) cmds);
        getCommand("packreload").setExecutor((CommandExecutor) cmds);
        
        // Register Listeners
        PackListener pack = PackListener.getInstance();
        getServer().getPluginManager().registerEvents(pack, this);
        
        clog.sendMessage(ChatColor.GREEN + "Pack Enabled");
	}
	
	@Override
	public void onDisable() 
	{
		super.onDisable();
		clog.sendMessage(ChatColor.RED + "Pack Disabled");
	}
}
